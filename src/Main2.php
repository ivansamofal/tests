<?php

namespace app;

class Main2
{
    private const LIGHT_PRICE      = 5.47;
    private const COLD_WATER_PRICE = 40.48;
    private const HOT_WATER_PRICE  = 198.19;

//    private const PREV_MONTH_LIGHT      = 15441;
//    private const PREV_MONTH_LIGHT      = 15525;
//    private const PREV_MONTH_LIGHT      = 15618;
    private const PREV_MONTH_LIGHT      = 15738;
//    private const PREV_MONTH_COLD_WATER = 681;
//    private const PREV_MONTH_COLD_WATER = 685;
//    private const PREV_MONTH_COLD_WATER = 688;
    private const PREV_MONTH_COLD_WATER = 694;
//    private const PREV_MONTH_HOT_WATER = 672;
//    private const PREV_MONTH_HOT_WATER = 677;
//    private const PREV_MONTH_HOT_WATER = 680;
    private const PREV_MONTH_HOT_WATER = 685;

//    private const CURRENT_MONTH_LIGHT = 15618;
//    private const CURRENT_MONTH_LIGHT = 15738;
    private const CURRENT_MONTH_LIGHT = 15857;
//    private const CURRENT_MONTH_COLD_WATER = 688;
//    private const CURRENT_MONTH_COLD_WATER = 694;
    private const CURRENT_MONTH_COLD_WATER = 699;
//    private const CURRENT_MONTH_HOT_WATER = 680;
//    private const CURRENT_MONTH_HOT_WATER = 685;
    private const CURRENT_MONTH_HOT_WATER = 689;

    public function calculate()
    {
        $light = (self::LIGHT_PRICE * (self::CURRENT_MONTH_LIGHT - self::PREV_MONTH_LIGHT));
        $coldWater = (self::COLD_WATER_PRICE * (self::CURRENT_MONTH_COLD_WATER - self::PREV_MONTH_COLD_WATER));
        $hotWater = (self::HOT_WATER_PRICE * (self::CURRENT_MONTH_HOT_WATER - self::PREV_MONTH_HOT_WATER));

        echo "Свет: $light, Холодная вода: $coldWater, Горячая вода: $hotWater\n";

        return 'Всего: ' . ($light + $coldWater + $hotWater);
    }

}

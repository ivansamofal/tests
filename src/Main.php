<?php

namespace app;

class Main
{
    private const HOST         = 'https://test3.bbp.one';
    private const HOST_BACKEND = 'https://backend3.bbp.one';
    //    private const HOST = 'https://stage-web.bankofpartners.com';
    private const LANG                  = '/ru';
    private const HTTP_STATUS_OK        = 200;
    private const VERIFICATION_FREE_URL = '/contractor/verification/verify-free?verificationId=:id';

    private $urls = [
        '/contractor/verification/get-all-free?countryId=160&companyUid=7736050003&companyName=',
        '/contractor/verification/get-all-pay?countryId=160&companyUid=7736050003&companyName=',
        '',
        '/contractor',
        '/service/search',
        '/site/bbp',
        '/badges',
        '/site/faq',
        '/site/info',
        '/user/profile',
        '/become-partner',
        '/analytics',
        '/site/chatbig',
        '/marketplaces',
        '/tender/search',
        '/events',
        '/user/edit-profile',
        '/user/accounts/index',
        '/user/accounts/edit-profile',
        '/user/accounts/create-account',
        '/user/documents/list',
        '/user/favorite',
        '/user/services',
        '/service/create',
        '/work-request/create',
        //        '/service/view?id=11143',
        '/user/profile?id=6451',
        '/work-request/view?id=122',
        '/user/saved-searches',


        //statistic
        //        '/statistic/profile-visitors?dateStart=2020-03-22&dateEnd=2020-04-21&offset=0&page=4',
        //        '/statistic/profiles?dateStart=2020-03-22&dateEnd=2020-04-21&offset=0&page=4',
        //        '/statistic/service?dateStart=2020-03-21&dateEnd=2020-05-01&serviceId=8540&offset=0',
        //        '/statistic/services-list?&offset=2&limit=4',
        //        '/statistic/requests-list?&offset=2&limit=4',
        //        '/statistic/request?dateStart=2020-04-01&dateEnd=2020-05-01&requestId=43&offset=0',
        //        '/statistic/countries?dateStart=2019-04-01&dateEnd=2020-04-02',
        //        '/statistic'

    ];

    private $backendUrls = [
        '/exportcenter/manual/task-proposal',
        '/exportcenter/manual/order-task',
        '/exportcenter/manual/order-task/update?id=1',
        '/user/list/index',
        '/user/orders/index',
        '/user/documents/index',
        '/user/block/index',
        '/user/payments',
        '/organization/index',
        '/service/index',
        '/work/index',
        '/seo',
        '/moderation',
        '/complaint',
        '/field-of-activity/index',
        '/skill/index',
        '/language-skills/index',
        '/link/index',
        '/country/index',
        '/country-region/index',
        '/city/index',
        '/currency/index',
        '/badge/index',
        '/marketplace/marketplace/index',
        '/marketplace/category/index',
        '/news/index',
        '/tender-aggregator/index',
        '/contractor/partner',
        '/contractor/partner/agreement',
        '/contractor/verification',
        '/contractor/verification/country',
        '/contractor/verification/language',
        '/contractor/verification/quick-sale/list',
        '/contractor/order',
        '/contractor/order/subscription/list',
        '/accelerator/common',
        '/accelerator/user/profiles',
        '/accelerator/actionplan/user/steps',
        '/accelerator/actionplan/user/orders',
        '/accelerator/milestones',
        '/accelerator/actionplan/steps',
        '/accelerator/actionplan/steps-videos',
        '/accelerator/actionplan/steps-events',
        '/accelerator/region-export',
        '/accelerator/partner-request',
        '/accelerator/pro-inn',
        '/quiz/quiz',
        '/quiz/answers',
        '/report/stat',
        '/statistics/users/countries',
        '/statistics/users/auth/providers',
        '/statistics/users/auth/provider?providerId=sberbankBusinessId',
        '/report/user',
        '/agreement/list',
        '/translatemanager',
        '/exportcenter/documents-files',
        '/blacklist/index',
        '/service-related-words/index',
    ];

    private $cookieKey = '2efq1f2q279df48rsphl2n88bk;';

    private $cookieKeyBackend = '420d8f6c070519f57db47b65bcb9f5ae;';

    private $response = [];

    public function check()
    {
        $str = '37.147.100.0 - - [10/Jun/2020:08:39:06 +0000] "GET /ru/contractor?utm_source=yandex&utm_medium=cpc&utm_campaign=bankofpartners_contractor_corporate_god_2020_context_search_general_rus_desk&utm_term=%D0%BF%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0%20%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D0%B0&utm_content=sm|45795451|4216866981|9170116362|20954871807||search|other|none|%d0%9c%d0%be%d1%81%d0%ba%d0%b2%d0%b0&yclid=3224357029818436922 HTTP/2.0" 200 30219 "https://yabs.yandex.ru/count/WbeejI_zOEu1XH80H20ETLrMyJqzb0K0xW8n5PpWNW00000u19pGcC08W07XuBMtqSgLsR41Y07_xuU8Jf01bFocgSI0W802c06K_AQfHBW1ogB2jH_00LpO0Vomu1hW0TZHj1he0OO1e0Buve4Ns082y0AgrEtv1lW2aF62vgN6kDNb0O03zQ29_mQ80vdTxUK2c0F9mJEW0mQe0nhrL_W4_QCTY0NzensG1QYT9g05klS8g0Nmk0Um1V2u1xW5YDm9m0N5i2V81R-12gGUyj0WEfvwqGQaHbs_8JgUUcS0002f1n-fm7UjChyii0U0W9Wyk0U01V47Avp6xyXWXDM020JG29I2lFF0OiH0CROGxp_92ZdzBcnCBFog2n3G-Pn_7wa004obeSMsfmK0sGle2_sZ7V0B1eWCshpUlW6f34AEz6lfGE0_w0mdu0s3W810YGw-C9ag69MifBt8Xut4Zizwe0x0X3tO3WMX3yFrA5oFmhK_-v3h5OWGp_Uu_mN0i13u41k04HsO4SAe1gEwqd8e73a_wH91ecL1la1OdF0I78WJ0TBjyOw8cxpUu1E8t0cW58ZS2QWKg9qcZiuSo1G2q1JppAOJs1IugT_s1UWK3D0LkAdVzWNO5S6AzkoZZxpyO_2O5j3OrVO5e1RGyF_s1SaMq1RQlDw-0O4Nc1VfrCiUg1S9m1SCs1V0X3qL03J9WLwG5PUj95yKvGfTdCX7YqXmwf1OP6V_7i-3f2elyyLNEvudWEWq4ZSTbI1T4BknWnjWUiuOYgY3FNs0hZeVggFZFzbE4m00~1?from=yandex.ru%3Bsearch%26%23x2F%3B%3Bweb%3B%3B0%3B&q=%D0%BF%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0+%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D0%B0+%D0%BF%D0%BE+%D0%B8%D0%BD%D0%BD&etext=2202.n8XXX-wZ39Oaf14F9wPcGTN_pYdKm7aIoZ_lmD0pj6h60e4E4EBdZgLFOKBQ4t4EGUTFsgdWXeKY2ul-BvLJQHh0a3RxZ3hnbWJvYXZ1YWE.8c37de569154314d9235626acdbc09e2b897c629&baobab_event_id=kb93q84xza" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36" "-" "bankofpartners.com" sn="bankofpartners.com" rt=0.314 ua="unix:/var/run/php-fpm/www.sock" us="200" ut="0.313" ul="239751" cs=-';


        var_dump(strlen($str));

        die;
        $this->checkUrls(true);
        $this->checkFreeVerifications();
        $this->checkPayVerifications();
        $this->checkProfileEdit();
        $this->checkCountryAutocomplete();
        $this->checkCityAutocomplete();
    }

    public function checkUrls($isBack = true)
    {
//        foreach ($this->urls as $url) {
//            $fullUrl = self::HOST . self::LANG . $url;
//            $this->executeResult($fullUrl);
//        }

        if ($isBack) {
            foreach ($this->backendUrls as $url) {
                $fullUrl = self::HOST_BACKEND . $url;
                $this->executeResult($fullUrl, 'backend');
            }
        }
    }

    /**
     * Бесплатные проверки конкретных партнеров
     */
    public function checkFreeVerifications()
    {
        foreach ([1, 2, 3, 4, 5, 6, 7, 12, 21] as $id) {
            $url = self::HOST . self::LANG . str_replace(':id', $id, self::VERIFICATION_FREE_URL);
            $this->executeFreeVerification($url);
        }
    }

    private function executeFreeVerification(string $url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => "VerificationSearchForm%5Bcountry_id%5D=204&VerificationSearchForm%5Bcompany_name%5D=&VerificationSearchForm%5Bcompany_uid%5D=%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F",
            CURLOPT_COOKIE         => "advanced-frontend={$this->cookieKey}",
            CURLOPT_HTTPHEADER     => [
                "content-type: application/x-www-form-urlencoded",
                "cookie: advanced-frontend={$this->cookieKey}",
            ],
        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if (intval($httpcode) !== self::HTTP_STATUS_OK) {
            var_dump("Error in page $url !!!");
        }

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $data = json_decode($response, true);
            if (!empty($data['name']) && $data['name'] == 'Exception') {
                var_dump("Error in URL $url");
            }
        }
    }

    /**
     * Платные проверки конкретных партнеров
     */
    public function checkPayVerifications()
    {
        $data = [
            8  => 3,
            11 => 17,
            15 => 36272,
            16 => 36274,
            17 => 34337,
            20 => 34760,
        ];
        foreach ($data as $verificationId => $countryId) {
            $curl = curl_init();
            echo 1;
            echo 1;

            $url = self::HOST . self::LANG . "/contractor/verification/get-pay-order-form?verificationId=$verificationId&verificationCountryLanguageId=$countryId";

            curl_setopt_array($curl, [
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_POSTFIELDS     => "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"VerificationSearchForm[country_id]\"\r\n\r\n160\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"VerificationSearchForm[company_uid]\"\r\n\r\n7736050003\r\n-----011000010111000001101001--\r\n",
                CURLOPT_COOKIE         => "advanced-frontend={$this->cookieKey}",
                CURLOPT_HTTPHEADER     => [
                    "content-type: multipart/form-data; boundary=---011000010111000001101001",
                    "cookie: advanced-frontend={$this->cookieKey}",
                ],
            ]);

            curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $err      = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } elseif ($httpcode != self::HTTP_STATUS_OK) {
                var_dump("Error in URL $url");
            }
        }
    }

    public function checkProfileEdit()
    {
        $curl = curl_init();
        $url  = self::HOST . self::LANG . "/user/accounts/edit-profile";

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[cuid]\"\r\n\r\n222661113\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[kpp]\"\r\n\r\n246356356356\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[ogrn]\"\r\n\r\n22222222222222\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[officialAddress]\"\r\n\r\n111\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[realAddress]\"\r\n\r\n222\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[headFullName]\"\r\n\r\n333\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[headPosition]\"\r\n\r\n444\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[authorityBasis]\"\r\n\r\n555\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[contactFullName]\"\r\n\r\n666\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[contactPosition]\"\r\n\r\n777\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[contactPhone]\"\r\n\r\n+262 35-3563-5638\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"OrganizationProfileForm[countryId]\"\r\n\r\n159\r\n-----011000010111000001101001--\r\n",
            CURLOPT_COOKIE         => "advanced-frontend={$this->cookieKey}",
            CURLOPT_HTTPHEADER     => [
                "content-type: multipart/form-data; boundary=---011000010111000001101001",
                "cookie: advanced-frontend={$this->cookieKey}",
            ],
        ]);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        var_dump('$httpcode ' . $httpcode);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }
    }

    public function checkCountryAutocomplete()
    {
        $hasError = true;
        $curl     = curl_init();
        $url      = self::HOST . self::LANG . '/country/get-list-with-country-codes?q=%D1%80%D0%BE%D1%81%D1%81';

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_POSTFIELDS     => "",
            CURLOPT_COOKIE         => "advanced-frontend={$this->cookieKey}",
        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $res = json_decode($response, true);
            if (!empty($res['results']) && !empty(current($res['results'])['id']) && current($res['results'])['id'] == 160) {
                $hasError = false;
            }
        }

        if ($hasError) {
            var_dump("Error in " . __METHOD__);
        }
    }

    public function checkCityAutocomplete()
    {
        $hasErrors = true;

        $curl = curl_init();
        $url  = self::HOST . self::LANG . '/city/get-list-for-autocomplete?country_code=RU&name=%D0%BC%D0%BE%D1%81%D0%BA';

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_POSTFIELDS     => "",
            CURLOPT_COOKIE         => "advanced-frontend={$this->cookieKey}",
        ]);

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $res = json_decode($response, true);
            if (!empty($res['results'])) {
                $hasErrors = false;
            }
        }

        if ($hasErrors) {
            var_dump("Error in " . __METHOD__);
        }
    }

    /**
     * @param string $url
     */
    private function executeResult(string $url, $host = 'frontend')
    {
        $cookieKeyHeader = "advanced-frontend={$this->cookieKey}";
        if ($host == 'backend') {
            $cookieKeyHeader = "advanced-backend=$this->cookieKeyBackend}";
        }
        $res                  = shell_exec("curl -H \"Cookie: $cookieKeyHeader\" --write-out \"%{http_code}\" --silent --output /dev/null $url");
        $res                  = (int) trim($res);
        $this->response[$url] = $res;
        if ($res !== self::HTTP_STATUS_OK) {
            var_dump("Error in page $url !!!");
        }
    }
}
